from django.contrib import admin
from django.urls import path
from courses_app.views import CourseView, CourseViewId

urlpatterns = [

    path('admin/', admin.site.urls),

    path('api/courses', CourseView.as_view()),
    path('api/courses/<int:pk>', CourseViewId.as_view())

]
