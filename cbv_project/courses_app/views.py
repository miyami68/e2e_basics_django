from rest_framework.views import APIView
from .service import Services


class course_view(APIView):
    def get(self, request):
        return Services.get_data(request)

    def post(self, request, *args, **kwargs):
        return Services.post_data(request, request.data)


class course_viewId(APIView):

    def get(self, request, pk):
        return Services.get_data_id(pk)

    def put(self, request, pk):
        return Services.put_data_id(pk, request.data)

    def delete(self, request, pk):
        return Services.delete_data_id(pk)
