from django.apps import AppConfig


class NewcbvConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'newcbv'
