from rest_framework import serializers
from .models import Course_Model


class CourseSerializers(serializers.ModelSerializer):
    class Meta:
        model = Course_Model
        fields = "__all__"
