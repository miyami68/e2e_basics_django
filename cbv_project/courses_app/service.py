from django.http import Http404
from rest_framework.response import Response
from rest_framework import status
from .models import Course_Model
from .serializers import CourseSerializers


class Services:

    def get_data(request):
        courses = Course_Model.objects.all()
        serializers = CourseSerializers(courses, many=True)
        return Response(serializers.data)

    def post_data(request, data):
        courseSerializer = CourseSerializers(data=data)
        if courseSerializer.is_valid():
            courseSerializer.save()
            return Response(courseSerializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(courseSerializer.error, status=status.HTTP_403_FORBIDDEN)

    def get_data_id(pk):
        try:
            course = Course_Model.objects.get(pk=pk)
            serializers = CourseSerializers(course, many=True)
            return Response(serializers.data, status.HTTP_200_OK)
        except course.DoesNotExit():
            raise Http404

    def put_data_id(pk, data):
        try:
            course = Course_Model.objects.get(pk=pk)
            courseSerializers = CourseSerializers(course, data=data)
            if courseSerializers.is_valid():
                courseSerializers.save()
                return Response(courseSerializers.data)
            return Response(courseSerializers.errors)
        except course.DoesNotExit():
            raise Http404

    def delete_data_id(pk):
        try:
            course = Course_Model.objects.get(pk=pk)
            course.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except course.DoesNotExit():
            raise Http404
