# from django.shortcuts import render
# from django.http import JsonResponse,HttpResponse
from django.http import HttpResponse
from .models import Employee
from .serializers import EmployeeSerializer,UserSerializer
from django.contrib.auth.models import User
# from django.views.decorators.csrf import csrf_exempt 
# from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

# Create your views here.
# @csrf_exempt
@api_view(['GET','POST'])
def employeeListView(request):
     if request.method=='GET':
        employee=Employee.objects.all()
        serializers=EmployeeSerializer(employee,many=True)
        return Response(serializers.data)
     elif request.method=='POST':
        serializers=EmployeeSerializer(data=request.data)
        if serializers.is_valid():
            serializers.save()
            return Response(serializers.data) 
        else:
            return Response(serializers.errors)



@api_view(['GET'])
def userListView(request):
    if request.method=='GET':
           users=User.objects.all()
           serializers=UserSerializer(users,many=True)
           return Response(serializers.data)


# @csrf_exempt
@api_view(['GET','PUT','DELETE'])
def employeeDetailView(request,pk):
           try:

               employee=Employee.objects.get(pk=pk)

           except Employee.DoesNotExist:
                
               return Response(status=404)

           except:

               return Response(status=404)


           if request.method=='DELETE':
               employee.delete()
               return Response(status.HTTP_204_NO_CONTENT)
           elif request.method=='GET':
               serializers=EmployeeSerializer(employee)
               return Response(serializers.data)
           elif request.method=='PUT':
                serializers=EmployeeSerializer(employee,data=request.data)
                if serializers.is_valid():
                  serializers.save()
                  return Response(serializers.data) 
                else:
                  return Response(serializers.errors)


def homeView(request):
    return   HttpResponse("just home page")