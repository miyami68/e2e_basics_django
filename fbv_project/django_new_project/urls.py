from django.contrib import admin
from django.urls import path
from employee_app.views import employeeListView, userListView, employeeDetailView, homeView

urlpatterns = [
    path('admin/', admin.site.urls, name='admin_url'),
    path('api/employees', employeeListView, name='employee_id_url'),
    path('api/users', userListView, name='user_id'),
    path('api/employees/<int:pk>', employeeDetailView, name='employee_id_url'),
    path('', homeView, name='home_url')
]
