# Generated by Django 4.1.5 on 2023-01-27 12:45

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='employee',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('salary', models.IntegerField()),
                ('doj', models.DateField()),
                ('department', models.CharField(max_length=40)),
            ],
        ),
    ]
