from django.views.generic import View
from .service import get_ordering
# Create your views here.


class employee_view(View):
    def get(self, request, *args, **kwargs):
        return get_ordering(request, args, kwargs)
