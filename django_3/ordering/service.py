from django.db.models import Max
from django.shortcuts import render
from .models import *
from django.template.response import TemplateResponse


def get_ordering(request, *args, **kwargs):
    # order by salary in asending order
    order_employee = EmployeeModel.objects.all().order_by('salary')
    print(order_employee)
    # order by desending
    order_employee = EmployeeModel.objects.all().order_by('-salary')
    print(order_employee)
    # printing perticular vlaue using order by
    order_by_employee_name = EmployeeModel.objects.all().order_by(
        '-salary').values_list('name', flat=True)
    print(order_by_employee_name)
    # ordering using annotate
    order_by_annotate_salary = EmployeeModel.objects.annotate(
        sal=Max('salary')).order_by('salary').values_list('name')
    print(order_by_annotate_salary)
    context = {}
    return TemplateResponse(request, 'employee.html', context)
