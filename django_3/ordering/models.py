from django.db import models


class EmployeeModel(models.Model):
    name = models.CharField(max_length=30)
    salary = models.IntegerField()
    doj = models.DateField()
    department = models.CharField(max_length=40)
