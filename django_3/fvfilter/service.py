from django.shortcuts import render
from django.views.generic import View
from .models import FieldValueModel
from django.db.models.functions import Substr
from django.db.models import Count
from django.db.models import F
from django.template.response import TemplateResponse


def get_fv(request, *args, **kwargs):
    get_query = FieldValueModel.objects.filter(f_name=F("l_name"))
    fvnew_query = FieldValueModel.objects.annotate(first=Substr(
        "f_name", 1, 1), last=Substr("l_name", 1, 1)).filter(first=F('last'))
    print(get_query.query)
    print("values")
    print(fvnew_query.query)
    # query set to test how to take values from duplicate field values
    duplicates_fv = FieldValueModel.objects.annotate(
        f_name_count=Count('f_name')).filter(f_name_count__gt=1)
    print(duplicates_fv.values())
    context = {}
    return TemplateResponse(request, 'fieldvalue.html', context)
