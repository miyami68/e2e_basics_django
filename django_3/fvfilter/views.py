from django.views.generic import View
from .service import get_fv
# Create your views here.


class FieldValueView(View):
    def get(self, request, *args, **kwargs):
        # query set to test F function
        return get_fv(request, args, kwargs)
