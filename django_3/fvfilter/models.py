from django.db import models

# Create your models here.


class FieldValueModel(models.Model):
    f_name = models.CharField(max_length=30)
    l_name = models.CharField(max_length=30)
    tax = models.IntegerField()
    rem_time_month = models.IntegerField()
