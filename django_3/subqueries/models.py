from django.db import models

# Create your models here.


class CategoryModel(models.Model):
    name = models.CharField(max_length=30)


class HeroModel(models.Model):
    name = models.CharField(max_length=30)
    category = models.ForeignKey(CategoryModel, on_delete=models.CASCADE)

    benevolence_factor = models.PositiveSmallIntegerField(
        help_text="how benevolent this hero is?",
        default=40
    )
