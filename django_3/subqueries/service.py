from django.shortcuts import render
from django.views.generic import View
from .models import *
from django.db.models import Subquery, OuterRef
from django.template.response import TemplateResponse


def get_subqueries(request, *args, **kwargs):
    # not able to understand
    sub_query = HeroModel.objects.filter(
        category=OuterRef("pk")
    ).order_by('-benevolence_factor')
    CategoryModel.objects.all().annotate(
        most_benevolent_hero=Subquery(sub_query.values('name')[:1]))
    print(sub_query.values().first().get('category_id'))
    context = {}
    return TemplateResponse(request, 'subquery.html', context)
