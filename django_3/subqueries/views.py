from django.views.generic import View
from .service import get_subqueries


class SubqueryView(View):
    def get(self, request, *args, **kwargs):
        return get_subqueries(request, args, kwargs)
