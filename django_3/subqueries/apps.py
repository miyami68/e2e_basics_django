from django.apps import AppConfig


class SubqueriesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'subqueries'
