from django.http import HttpResponse
from .service import get_books
# Create your views here.


def homeView(request, *args, **kwargs):
    return HttpResponse("<h1> this is HttpResponse </h1>")


def booksView(request, *args, **kwargs):
    # to see what happen at the time of model.objects.all()
    if request.method == 'GET':
        return get_books(request, args, kwargs)
