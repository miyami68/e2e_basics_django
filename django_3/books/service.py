from django.shortcuts import render
from .models import BooksModel
from django.db.models import Q
from django.template.response import TemplateResponse


def get_books(request, *args, **kwargs):
    books_query_set = BooksModel.objects.all()
    print(books_query_set)
    print(books_query_set.query)
    print(books_query_set.values())
    # to check how filter work and what it return
    books_query_set_filter = BooksModel.objects.filter(id__gt=3)
    print(books_query_set_filter)
    print(books_query_set_filter.query)
    print(books_query_set_filter.values())
    # to check or with filter and Q
    books_query_set_with_or = BooksModel.objects.filter(
        name__startswith='p') | BooksModel.objects.filter(name__endswith='g')
    books_query_set_with_or_Q = BooksModel.objects.filter(
        Q(name__startswith='p') | Q(name__endswith='g'))
    print(books_query_set_with_or)
    print(books_query_set_with_or.query)
    print(books_query_set_with_or.values())
    print(books_query_set_with_or_Q.values)
    # not operation with exclude and Q
    books_query_set_with_negation_exclude = BooksModel.objects.exclude(
        id__gt=3)
    books_query_set_with_negation_Q = BooksModel.objects.filter(
        ~Q(id__gt=3))
    print(books_query_set_with_negation_exclude)
    print(books_query_set_with_negation_exclude.query)
    print(books_query_set_with_negation_Q)
    print(books_query_set_with_negation_Q.exclude)
    # Union operation
    # union can perform on same field
    books_query_set_q1 = BooksModel.objects.filter(id__gt=4)
    books_query_set_q2 = BooksModel.objects.filter(id__lt=2)
    value1 = books_query_set_q1.union(books_query_set_q2)
    value2 = books_query_set_q2.union(books_query_set_q1)
    print(books_query_set_q1)
    print(books_query_set_q2)
    print(value1.query)
    print(value2.query)
    # working with only and values function
    books_query_set_only = BooksModel.objects.filter(
        price__gt=30).only("price", "discount")
    books_query_set_value = BooksModel.objects.filter(
        price__gt=30).values("price", "discount")
    print(books_query_set_only)
    print(books_query_set_value)
    print(books_query_set_only.values())
    print(books_query_set_value.values())

    return TemplateResponse(request, 'books.html', {})
