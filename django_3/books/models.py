from django.db import models

# Create your models here.
class BooksModel(models.Model):
    name=models.CharField(max_length=30)
    price=models.IntegerField()
    author=models.CharField(max_length=30)
    discount=models.FloatField()
    description=models.TextField(max_length=300)




