from django.contrib import admin
from django.urls import path
from books.views import homeView
from books.views import booksView
from relationships.views import relation_views
from ordering.views import employee_view
from subqueries.views import SubqueryView
from teacher_course.views import teacher_course_views
from fvfilter.views import FieldValueView
urlpatterns = [
    path('admin/', admin.site.urls),
    path('books/', booksView, name='books'),
    path('relation/', relation_views.as_view(), name='realtion'),
    path('employee/', employee_view.as_view(), name='employee'),
    path('teacher-course/', teacher_course_views.as_view(), name='teacher_course'),
    path('subquery/', SubqueryView.as_view(), name='subquery'),
    path('fieldvalue/', FieldValueView.as_view(), name='fieldvalue'),
    path('', homeView, name='Home'),
]
