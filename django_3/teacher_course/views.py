from django.views.generic import View
from .service import get_teacher_course
# Create your views here.


class teacher_course_views(View):
    def get(self, request, *args, **kwargs):
        return get_teacher_course(request, args, kwargs)
