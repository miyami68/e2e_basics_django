from django.apps import AppConfig


class TeacherCourseConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'teacher_course'
