from django.db import models

# Create your models here.


class TeacherModel(models.Model):
    name = models.CharField(max_length=20)


class CourseModel(models.Model):
    name = models.CharField(max_length=30)
    teacher = models.ForeignKey(TeacherModel, on_delete=models.CASCADE)
