from django.shortcuts import render
from django.views.generic import View
from .models import *
from django.template.response import TemplateResponse


def get_teacher_course(request, *args, **kwargs):
    # order by two field
    # nahi ho raha
    teacher_order_by_course = TeacherModel.objects.all().order_by(
        'coursemodel__name', 'name'
    )
    print(teacher_order_by_course.values())
    # not able to understoop
    course_order_by_teacher = CourseModel.objects.all().order_by(
        'name'
    )
    print(course_order_by_teacher.values())
    # print(course_order_by_teacher.teacher)
    context = {}
    return TemplateResponse(request, 'teacher-course.html', context)
