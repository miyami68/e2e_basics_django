from django.db import models


class InterestModel(models.Model):
    name = models.CharField(max_length=30)


class CityModel(models.Model):
    name = models.CharField(max_length=30)


class PersonModel(models.Model):
    name = models.CharField(max_length=30)
    phoneNo = models.IntegerField()
    interests = models.ManyToManyField(InterestModel)


class PersonalAddressModel(models.Model):
    place = models.CharField(max_length=30)
    city = models.ForeignKey(CityModel, on_delete=models.CASCADE)
    person = models.OneToOneField(PersonModel, on_delete=models.CASCADE)
