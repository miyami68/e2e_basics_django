from django.shortcuts import render
from .models import *
from django.template.response import TemplateResponse


def get_relationships(request, *args, **kwargs):
    # accessing person model with their interest (forward addressing)
    person_query_set = PersonModel.objects.filter(id=1).first()
    print(person_query_set.interests.all())
    # print(person_query_set.query)
    # print(person_query_set.query)
    # first does not have .query method
    # accesssing cities from personal address
    address_query_set = PersonalAddressModel.objects.filter(id=2)
    print(address_query_set.values)
    # accessing person from address
    address_query_set_person = PersonalAddressModel.objects.filter(
        id=2).first()
    print(address_query_set_person.person.pk)
    # reverse query in models
    city_reverse = CityModel.objects.get(id=2)
    address = city_reverse.personaladdressmodel_set.all()
    print("ldfjdsl")
    print(address.values())
    # person = address.personmodel_set.all()
    # can we access person (here address is access through city and now i want to access person from addresss)
    # print(person)
    # print(person_reverse_query.citymodel__set.all())
    interest = InterestModel.objects.get(id=3)
    person = interest.personmodel_set.all()
    print("lsdfjl")
    print(person)
    context = {}
    return TemplateResponse(request, 'relationship.html', context)
