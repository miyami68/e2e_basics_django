from django.views.generic import View
from .service import get_relationships
# Create your views here.


class relation_views(View):
    def get(self, request, *args, **kwargs):
        return get_relationships(request, args, kwargs)
