# Generated by Django 4.1.5 on 2023-01-27 12:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('relationships', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='personmodel',
            name='phoneNo',
            field=models.IntegerField(),
        ),
    ]
