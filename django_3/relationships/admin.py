from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register([InterestModel, PersonModel,
                    PersonalAddressModel, CityModel])
