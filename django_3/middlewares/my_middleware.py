import time


class DateTimeMiddleware:
    no_req_res = 0

    def __init__(self, get_response):
        self.st = time.time()
        self.et = 0
        self.get_response = get_response

    def __call__(self, request, *args, **kwargs):
        self.no_req_res += 1
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, *view_args, **view_kwargs):
        return None

    def process_exception(self, request, exception):
        pass

    def process_template_response(self, request, response):
        self.et = time.time()
        print(self.et-self.st)
        print(self.no_req_res)
        return response
